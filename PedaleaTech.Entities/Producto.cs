﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PedaleaTech.Entities
{
    public class Producto
    {
        public Guid Id_producto { get; set; }
        public string Nombre { get; set; }

        // otra opción es tener una lista de precios dependiendo las politicas de la empresa
        public double Precio { get; set; }
        public string Descripcion { get; set; }
        public string Nombre_Imagen { get; set; }

    }
}
