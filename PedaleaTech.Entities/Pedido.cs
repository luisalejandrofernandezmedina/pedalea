﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PedaleaTech.Entities
{
    public class Pedido
    {
        public Guid Id_pedido { get; set; }
        public string Codigo_pedido { get; set; }
        public Cliente Cliente { get; set; }
        public Guid Id_Producto { get; set; }
        public int Id_cliente { get; set; }
        public DateTime Fecha_pedido { get; set; }
        public string Direccion_entrega { get; set; }
        public decimal Total_pedido { get; set; }
        public int Cantidad { get; set; }
    }
}
