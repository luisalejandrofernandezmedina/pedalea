﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pedalea.Data.Dto
{
    public class ProductoEnCarroDto
    {
        public string Nombre { get; set; }
        public int Cantidad { get; set; }
    }
}
