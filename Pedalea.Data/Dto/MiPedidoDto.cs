﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pedalea.Data.Dto
{
    public class MiPedidoDto
    {
        public Guid Id_producto { get; set; }
        public string Nombre { get; set; }
        public decimal Precio { get; set; }
        public string Codigo_pedido { get; set; }
        public int Id_cliente { get; set; }
        public DateTime Fecha_pedido { get; set; }
        public int Cantidad { get; set; }
        public decimal Total_pedido { get; set; }
        public string Direccion_entrega { get; set; }
    }
}
