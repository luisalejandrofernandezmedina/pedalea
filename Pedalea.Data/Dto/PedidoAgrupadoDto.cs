﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pedalea.Data.Dto
{
    public class PedidoAgrupadoDto
    {
        public string Codigo_pedido { get; set; }
        public ProductoPedidoDto[] Productos { get; set; }
        public DateTime Fecha_pedido { get; set; }
        public decimal Total_pedido { get; set; }
        public string Direccion_entrega { get; set; }

    }
}
