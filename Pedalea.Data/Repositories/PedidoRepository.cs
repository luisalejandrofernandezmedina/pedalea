﻿using Dapper;
using Pedalea.Data.Dto;
using Pedalea.Data.Repositories.Interface;
using PedaleaTech.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pedalea.Data.Repositories
{
    public class PedidoRepository : IPedidoRepository
    {
        public bool EliminarPedido(string codigo)
        {
            using (var connect = new SqlConnection(ConfigurationManager.AppSettings["PedaleaConnect"].ToString()))
            {
                var res = connect.Query<int>("USP_ProcesosPedido"
                     , new { CODIGO_PEDIDO = codigo, TRANSACTION = 3 }
                     , commandType: CommandType.StoredProcedure).ToList().FirstOrDefault();

                return res == 0 ? true : false; ;
            }
        }

        public int GuardarPedido(Pedido pedido)
        {
            using (var connect = new SqlConnection(ConfigurationManager.AppSettings["PedaleaConnect"].ToString()))
            {
                var res = connect.Execute("USP_ProcesosPedido"
                    ,new { 
                        ID = new Guid(),
                        ID_CLIENTE = pedido.Id_cliente,
                        ID_PRODUCTO = pedido.Id_Producto,
                        FECHA_PEDIDO = DateTime.Now,
                        TOTAL_PEDIDO = pedido.Total_pedido,
                        DIRECCION_ENTREGA = pedido.Direccion_entrega,
                        CODIGO_PEDIDO = pedido.Codigo_pedido,
                        CANTIDAD = pedido.Cantidad,
                        TRANSACTION = 2
                    }
                    , commandType: CommandType.StoredProcedure);
                return res;
            }
        }

        public List<MiPedidoDto> ObtenerMisPedidos(int Id_cliente)
        {
            using (var connect = new SqlConnection(ConfigurationManager.AppSettings["PedaleaConnect"].ToString()))
            {
                var res = connect.Query<MiPedidoDto>("USP_ProcesosPedido"
                     , new{ID_CLIENTE = Id_cliente, TRANSACTION = 1}
                     , commandType: CommandType.StoredProcedure).ToList();
                return res;
            }
        }
    }
}
