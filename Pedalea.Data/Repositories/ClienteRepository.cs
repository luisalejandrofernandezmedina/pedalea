﻿using Dapper;
using Pedalea.Data.Repositories.Interface;
using PedaleaTech.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pedalea.Data.Repositories
{
    public class ClienteRepository : IClienteRepository
    {
        public Cliente BuscarClientePorIdentificacion(int identificacion)
        {
            using (var connect = new SqlConnection(ConfigurationManager.AppSettings["PedaleaConnect"].ToString()))
            {
                var res = connect.Query<Cliente>("USP_ObtenerClientePorCedula", new { CEDULA = identificacion },commandType: CommandType.StoredProcedure).FirstOrDefault();
                return res;
            }
        }

        public Cliente GuardarCliente(Cliente cliente)
        {
            throw new NotImplementedException();
        }
    }
}
