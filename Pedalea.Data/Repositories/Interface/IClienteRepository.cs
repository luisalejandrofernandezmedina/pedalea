﻿using PedaleaTech.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pedalea.Data.Repositories.Interface
{
    public interface IClienteRepository
    {
        public Cliente GuardarCliente(Cliente cliente);
        public Cliente BuscarClientePorIdentificacion(int identificacion);
    }
}
