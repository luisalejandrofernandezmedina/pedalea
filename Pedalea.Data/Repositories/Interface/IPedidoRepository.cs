﻿using Pedalea.Data.Dto;
using PedaleaTech.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pedalea.Data.Repositories.Interface
{
    public interface IPedidoRepository
    {
        int GuardarPedido(Pedido pedido);
        List<MiPedidoDto> ObtenerMisPedidos(int Id_cliente);
        bool EliminarPedido(string codigo);

    }
}
