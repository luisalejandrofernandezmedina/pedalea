﻿using Dapper;
using Pedalea.Data.Repositories.Interface;
using PedaleaTech.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pedalea.Data.Repositories
{
    public class ProductoRepository : IProductoRepository
    {
        public List<Producto> ObtenerProductos()
        {
            using (var connect = new SqlConnection(ConfigurationManager.AppSettings["PedaleaConnect"].ToString()))
            {
                var res = connect.Query<Producto>("USP_ObtenerProductos", commandType: CommandType.StoredProcedure).ToList();
                return res;
            }
           
        }
    }
}
