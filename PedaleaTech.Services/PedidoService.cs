﻿using Pedalea.Data.Dto;
using Pedalea.Data.Repositories.Interface;
using PedaleaTech.Entities;
using PedaleaTech.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PedaleaTech.Services
{
    public class PedidoService : IPedidoService
    {
        private readonly IPedidoRepository _pedidoRepository;
        private readonly IClienteRepository _clienteRepository;
        private readonly IProductoRepository _productoRepository;

        public PedidoService(IPedidoRepository pedidoRepository
            , IClienteRepository clienteRepository
            ,IProductoRepository productoRepository)
        {
            _pedidoRepository = pedidoRepository;
            _clienteRepository = clienteRepository;
            _productoRepository = productoRepository;
        }

        public bool EliminarPedido(string codigo)
        {
            return _pedidoRepository.EliminarPedido(codigo);
        }

        public int Guardar(int identificacion, Pedido pedido, ProductoEnCarroDto[] nombresProductos)
        {
            Cliente cliente = _clienteRepository.BuscarClientePorIdentificacion(identificacion);
            int res = 0;
            if (cliente != null)
            {
                pedido.Id_cliente = cliente.Identificacion;
                string guid = Guid.NewGuid().ToString();
                pedido.Codigo_pedido = $"codp{guid}";
                foreach (var productos in nombresProductos)
                {
                    pedido.Id_Producto = _productoRepository.ObtenerProductos()
                        .FirstOrDefault(p => p.Nombre == productos.Nombre).Id_producto;
                    pedido.Cantidad = productos.Cantidad;
                   res = _pedidoRepository.GuardarPedido(pedido);
                }
                
            }
            return res;
        }

        public List<PedidoAgrupadoDto> ObtenerMisPedidos(int Id_cliente)
        {
            var pedidos = _pedidoRepository.ObtenerMisPedidos(Id_cliente);

            var agrupados = pedidos.GroupBy(p => p.Codigo_pedido).ToList();

            List<PedidoAgrupadoDto> listaPedidosAgrupados = new();
            
            foreach (var group in agrupados)
            {
                List<ProductoPedidoDto> listaprod = new();
                foreach (var pedido in group)
                {
                    listaprod.Add(new ProductoPedidoDto
                    {
                        Nombre = pedido.Nombre,
                        Cantidad = pedido.Cantidad,
                        Descripcion = "",
                        Precio  = pedido.Precio
                    });
                }
                var pedidoFirst = group.FirstOrDefault();
                listaPedidosAgrupados.Add(new PedidoAgrupadoDto
                {
                    Productos = listaprod.ToArray(),
                    Codigo_pedido = group.Key,
                    Direccion_entrega = pedidoFirst.Direccion_entrega,
                    Fecha_pedido = pedidoFirst.Fecha_pedido,
                    Total_pedido = pedidoFirst.Total_pedido
                });
            }

            return listaPedidosAgrupados;
        }
    }
}
