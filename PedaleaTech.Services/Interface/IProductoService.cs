﻿using PedaleaTech.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PedaleaTech.Services.Interface
{
    public interface IProductoService
    {
        public List<Producto> ObtenerProductos();
    }
}
