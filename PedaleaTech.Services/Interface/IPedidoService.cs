﻿using Pedalea.Data.Dto;
using PedaleaTech.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PedaleaTech.Services.Interface
{
    public interface IPedidoService
    {
        int Guardar(int identificacion, Pedido pedido, ProductoEnCarroDto[] nombresProductos);
        bool EliminarPedido(string codigo);
        List<PedidoAgrupadoDto> ObtenerMisPedidos(int Id_cliente);
    }
}
