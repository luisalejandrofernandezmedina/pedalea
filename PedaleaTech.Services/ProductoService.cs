﻿using Pedalea.Data.Repositories.Interface;
using PedaleaTech.Entities;
using PedaleaTech.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PedaleaTech.Services
{
    public class ProductoService : IProductoService
    {
        private readonly IProductoRepository _productoRepository;

        public ProductoService(IProductoRepository productoRepository)
        {
            _productoRepository = productoRepository;
        }
        public List<Producto> ObtenerProductos()
        {
            return _productoRepository.ObtenerProductos();
        }
    }
}
