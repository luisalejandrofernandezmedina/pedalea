﻿using Pedalea.Data.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PedaleaTech.Dto
{
    public class PedidoDto
    {
        public int Cedula_cliente { get; set; }
        public string Direccion_entrega { get; set; }
        public ProductoEnCarroDto[] ProductosEnCarro { get; set; }
        public decimal Total_pedido { get; set; }
        public int Cantidad { get; set; }
    }
}
