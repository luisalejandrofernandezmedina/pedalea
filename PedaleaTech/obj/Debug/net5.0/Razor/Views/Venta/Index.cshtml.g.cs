#pragma checksum "C:\Users\felig\Documents\Pedalea Repo\PedaleaTech\Views\Venta\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "19cbf5c4cef8324919003d3b7816365399fa2330"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Venta_Index), @"mvc.1.0.view", @"/Views/Venta/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"19cbf5c4cef8324919003d3b7816365399fa2330", @"/Views/Venta/Index.cshtml")]
    #nullable restore
    public class Views_Venta_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    #nullable disable
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\felig\Documents\Pedalea Repo\PedaleaTech\Views\Venta\Index.cshtml"
  
    Layout = "~/Views/Shared/_MasterPage.cshtml";
    var productos = (List<PedaleaTech.Dto.ProductoDto>)ViewBag.Productos;


#line default
#line hidden
#nullable disable
            WriteLiteral(@"<div class=""carrito"">
    <button type=""button"" class=""btn btn-primary position-relative rigth"" data-bs-toggle=""offcanvas"" href=""#offcanvasCarrito"" role=""button"" aria-controls=""offcanvasCarrito"">
        <i class=""bi bi-cart4""></i>
        <span id=""cantidadProductos"" class=""position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger"">
            0
            <span class=""visually-hidden"">unread messages</span>
        </span>
    </button>
</div>
<br />
<div class=""row"">
    <div class=""col-sm-10"">
        <div>
            ");
#nullable restore
#line 19 "C:\Users\felig\Documents\Pedalea Repo\PedaleaTech\Views\Venta\Index.cshtml"
       Write(Html.Partial("~/Views/Shared/Producto/_ProductosPartial.cshtml", productos));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </div>\r\n    </div>\r\n\r\n");
            WriteLiteral(@"

    <div class=""col-sm-2"">
        <div class=""card"" style=""width: 18rem;"">
            <div class=""card-body"">
                <form method=""get"" action=""/mipedido/index"">
                    <h5 class=""card-title"">Consulta tus pedidos aquí</h5>
                    <p class=""card-text"">

                        <div class=""form-floating mb-3"">
                            <input type=""text"" class=""form-control"" required id=""cedula"" name=""identificacion"">
                            <label for=""cedula"">Cédula</label>
                        </div>


                    </p>
                    <button class=""btn btn-primary"" type=""submit"">Consultar pedidos</button>
                </form>
            </div>
        </div>

        <div class=""offcanvas offcanvas-end"" tabindex=""-1"" id=""offcanvasCarrito"" aria-labelledby=""offcanvasCarritoLabel"">
            <div class=""offcanvas-header"">
                <h5 class=""offcanvas-title"" id=""offcanvasCarritoLabel"">Mi Carrito </h5>
             ");
            WriteLiteral(@"   <button type=""button"" class=""btn-close text-reset"" data-bs-dismiss=""offcanvas"" aria-label=""Close""></button>
            </div>
            <div class=""offcanvas-body"">
                <div id=""Carro"">
                    <div class=""row"" id=""listaProductos"">

                    </div>

                    <div class=""footerCarro"">
                        <button id=""realizaPedido"" class=""btn btn-primary"" disabled data-bs-toggle=""modal"" data-bs-target=""#confirmarPedido"">
                            Realizar Pedido
                        </button>
                        <div class=""rigth"">
                            <strong>Total-compra: </strong>
                            <label id=""totalCompra""></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>

<!-- Modal -->
<div class=""modal fade"" id=""confirmarPedido"" tabindex=""-1"" aria-labelledby=""confirmarPedido"" aria-hidden=""true"">
    <div cl");
            WriteLiteral(@"ass=""modal-dialog"">
        <div class=""modal-content"">
            <div class=""modal-header"">
                <h5 class=""modal-title"" id=""confirmarPedidoLabel"">Falta poco para realizar tu pedido!</h5>
                <button type=""button"" class=""btn-close"" data-bs-dismiss=""modal"" aria-label=""Close""></button>
            </div>
            <div class=""modal-body"">
                <div class=""form-floating mb-3"">
                    <input type=""text"" class=""form-control"" id=""cedulaClientePedido"" placeholder=""c.c"">
                    <label for=""cedulaClientePedido"">Cédula:</label>
                </div>
                <div class=""form-floating"">
                    <input type=""text"" class=""form-control"" id=""direccionEntrega"" placeholder=""Calle 131b#154-22"">
                    <label for=""direccionEntrega"">Dirección de entrega</label>
                </div>
            </div>
            <div class=""modal-footer"">
                <button type=""button"" class=""btn btn-secondary"" data-bs-dism");
            WriteLiteral(@"iss=""modal"">Cancelar</button>
                <button type=""button"" onclick=""confirmarPedido()"" class=""btn btn-primary"">Confirmar Pedido</button>
            </div>
        </div>
    </div>
</div>

<div class=""position-fixed bottom-0 end-0 p-3"" style=""z-index: 11"">
    <div id=""liveToast"" class=""toast"" role=""alert"" aria-live=""assertive"" aria-atomic=""true"">
        <div class=""toast-header"">
            <strong class=""me-auto"">Nuevo producto</strong>
            <button type=""button"" class=""btn-close"" data-bs-dismiss=""toast"" aria-label=""Close""></button>
        </div>
        <div class=""toast-body"">
            Agregaste un nuevo producto al carrito!
        </div>
    </div>
</div>");
        }
        #pragma warning restore 1998
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; } = default!;
        #nullable disable
    }
}
#pragma warning restore 1591
