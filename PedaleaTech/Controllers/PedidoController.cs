﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PedaleaTech.Dto;
using PedaleaTech.Entities;
using PedaleaTech.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PedaleaTech.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]    
    public class PedidoController : Controller
    {
        private readonly IPedidoService _pedidoService;
        private readonly IMapper _mapper;
        public PedidoController(IPedidoService pedidoService, IMapper mapper)
        {
            _pedidoService = pedidoService;
            _mapper = mapper;
        }

        [HttpPost()]
        public ActionResult<IEnumerable<PedidoDto>> GuardarPedido([FromBody] PedidoDto pedidoDto)
        {
            try
            {
                return Ok(_pedidoService
                    .Guardar(pedidoDto.Cedula_cliente
                    , _mapper.Map<Pedido>(pedidoDto)
                    , pedidoDto.ProductosEnCarro));
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
        [HttpGet()]
        public ActionResult<IEnumerable<bool>> EliminarPedido(string codigo)
        {
            try
            {
                return Ok(_pedidoService.EliminarPedido(codigo));
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
    }
}
