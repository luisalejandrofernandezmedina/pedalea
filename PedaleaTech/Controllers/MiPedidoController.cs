﻿using Microsoft.AspNetCore.Mvc;
using PedaleaTech.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PedaleaTech.Controllers
{
    [Route("[controller]/[action]")]
    public class MiPedidoController : Controller
    {
        private readonly IPedidoService _pedidoService;

        public MiPedidoController(IPedidoService pedidoService)
        {
            _pedidoService = pedidoService;
        }
        [HttpGet()]
        public ActionResult Index(int identificacion)
        {
            var pedidos = _pedidoService.ObtenerMisPedidos(identificacion);
            ViewBag.MisPedidos = pedidos;
            return View();
        }
    }
}
