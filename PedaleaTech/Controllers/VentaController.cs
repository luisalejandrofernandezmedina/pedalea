﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PedaleaTech.Dto;
using PedaleaTech.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PedaleaTech.Controllers
{
    [Route("[controller]/[action]")]
    public class VentaController : Controller
    {
        private readonly IProductoService _productoService;
        private readonly IMapper _mapper;

        public VentaController(IProductoService productoService, IMapper mapper)
        {
            _productoService = productoService;
            _mapper = mapper;
        }

        [HttpGet()]
        public ActionResult Index()
        {
             
           ViewBag.Productos = _mapper.Map<List<ProductoDto>>(_productoService.ObtenerProductos());
           
            return View();
        }

    }
}
