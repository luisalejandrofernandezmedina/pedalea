﻿using Microsoft.AspNetCore.Mvc;
using PedaleaTech.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PedaleaTech.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class ProductoController : Controller
    {
        private readonly IProductoService _productoService;

        public ProductoController(IProductoService productoService)
        {
            _productoService = productoService;
        }

        [HttpGet()]
        public ActionResult<IEnumerable<Object>> ObtenerProductos()
        {
            try
            {
                var data = _productoService.ObtenerProductos();
                return Ok(data);
            }
            catch (Exception ex)
            {
                //if (ex.GetType() != typeof(SqlException))
                //    _logger.LogError(JsonConvert.SerializeObject(ex));
                return Problem();
            }
        }
    }
}
