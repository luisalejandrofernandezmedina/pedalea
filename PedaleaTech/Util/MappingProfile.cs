﻿using AutoMapper;
using PedaleaTech.Dto;
using PedaleaTech.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PedaleaTech.Util
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Producto, ProductoDto>();
            CreateMap<PedidoDto, Pedido>();
        }
        
    }
}
