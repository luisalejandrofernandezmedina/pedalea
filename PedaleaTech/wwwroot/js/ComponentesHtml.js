﻿let componentProductoEnCarro = (nombre, precio) => {
    return `<div id="${nombre}-cod-prod" class="productoEnCarro col-sm-12">
    <strong> Nombre:</strong>
    <label>${nombre}</label>
    <strong>Precio:</strong>
    <label>${precio}</label>
    <input id="input-cod-${nombre}-producto" type="number" onchange="actualizarCantidadPorProducto('${nombre}')"
        onkeyup="actualizarCantidadPorProducto('${nombre}')" value="1" pattern="^[0-1000]" min="1" style="width:70px" class="form-control" />
    <button type="button" onclick="eliminarProductoDelCarro('${nombre}')" class="btn btn-danger">
    <i class="bi bi-trash"></i>
    </button>
    </div >`;
}

let componentAlertAgregado = () => {
    return `<div style="margin-top: 15px;" class="alert alert-warning" role="alert">
            Ya agregaste este producto al carrito!
            </div>`
}