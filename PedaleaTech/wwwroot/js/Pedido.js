﻿
let codigoPedido = "";

function eliminarPedido(codigo) {
    codigoPedido = codigo;
    $('#pedidoId').html(codigo);
    $("#mensajeEliminar").html(`¿Quieres eliminar el pedido <span id="pedidoId">${codigo}</span>?`);
    $('#eliminarPedido').modal('show');
}

function confirmarEliminarPedido() {

    $.ajax({
        method: "GET",
        url: URI_ELIMNARPEDIDO + "?codigo=" + codigoPedido,
        contentType: "application/json"
    }).done(function (data) {
        $('#estadoEliminar').html('<img src="../Images/correcto.png" style="width: 100px;" />');
        $("#mensajeEliminar").html(`Se eliminó correctamente`);
        $('#misPedidos').load(location.href + " #misPedidos");
        setTimeout(() => {
            $('#eliminarPedido').modal('hide');
            $('#estadoEliminar').html("");
        }, 2000);
    }).fail(function () {
        alert("Algo salió mal");
    });
}