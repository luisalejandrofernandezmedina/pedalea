﻿
let arregloProductos = [];
let totalCompra = 0;


function agregarProducto(nombre, precio) {
    let codigoProducto = "cod-" + nombre + "-producto";
    let res = arregloProductos.find(prod => prod.codigo === codigoProducto);
    if (res == undefined) {
        arregloProductos.push({ nombre: nombre, precio: precio, codigo: codigoProducto, cantidad: 1 });
        var html = componentProductoEnCarro(nombre, precio);

        var toast = new bootstrap.Toast(document.getElementById('liveToast'))
        toast.show()

        selectorListaCarro.append(html);
    } else {
        selectorAlert(nombre).append(componentAlertAgregado);
        setTimeout(() => selectorAlert(nombre).html(""), 4000);
    }
    calcularTotalCompra();
    validarRealizarCompra();
    mostrarCantidad();
}

function actualizarCantidadPorProducto(nombre) {
    arregloProductos.forEach(p => {
        if (p.codigo === "cod-" + nombre +"-producto") {
            p['cantidad'] = parseInt($(`#input-cod-${nombre}-producto`).val());
        }
    });
    calcularTotalCompra();
    validarRealizarCompra();
}

function calcularTotalCompra() {
    totalCompra = 0;
    arregloProductos.forEach(p => {
        totalCompra = parseInt(totalCompra) + (parseInt(p['cantidad']) * parseInt(p['precio'])) ;
    });
    selectorTotalCompra.html("<span>" + new Intl.NumberFormat('es-CO').format(totalCompra) + "</span>");
    validarRealizarCompra();
}

function eliminarProductoDelCarro(nombre) {
    let codigo = `cod-${nombre}-producto`;
    arregloProductos = arregloProductos.filter((p) => p.codigo !== codigo);
    $(`#${nombre}-cod-prod`).remove();
    calcularTotalCompra();
    validarRealizarCompra();
    mostrarCantidad();
}

function validarRealizarCompra() {
    if (arregloProductos.length > 0) {
        selectorRealizarPedido.removeAttr("disabled");
    } else {
        selectorRealizarPedido.attr("disabled");
    }
}

function confirmarPedido() {
    let productosEnCarro = [];
    arregloProductos.forEach(p => {
        productosEnCarro.push({ nombre: p['nombre'], cantidad: p['cantidad'] })
    });
    
    var pedidoDto = {
        "productosEnCarro": productosEnCarro,
        "total_pedido": parseInt(totalCompra),
        "direccion_entrega": selectorDireccionEntrega.val(),
        "cedula_cliente": parseInt(selectorCedulaClientePedido.val())
    }

    $.ajax({
        method: "POST",
        url: URI_CONFIRMAPAGO,
        data: JSON.stringify(pedidoDto),
        contentType: "application/json",
        dataType:'json'
    }).done(function (data) {
        if (data == 1) {
            $('#imgMensaje').html('<img src="../Images/correcto.png" style="width: 100px;" />');
            $("#mensajePedidoTxt").html(`Se registró el pedido correctamente`);
            setTimeout(() => location.reload(), 2000);
        }
    }).fail(function () {
        alert("Algo salió mal");
    });
}


function mostrarCantidad() {
    selectorCantidadCarrito.html(arregloProductos.length);
}
